import React, { useState } from 'react';
import { Animated, StyleSheet, TouchableHighlight, CheckBox, View, Text, FlatList, Alert, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import Swipeout from 'react-native-swipeout';

const Todo = (props) => {

    const [isRefreshing, setRefreshing] = useState(false);


    const [isSelected, setSelection] = useState(false);

    function componentWillMount() {

        this.props.getAll();

    }


    const renderItem = ({ item }) => {


        const checkTaskComplete = () => {
            item.complete = !item.complete
        }

        const swipeOutButton = [
            {
                text: 'Delete',
                backgroundColor: 'red',
                borderRadius: 10,
                borderWidth: 1
            }
        ]


        return (

            <Swipeout
                right={swipeOutButton}
                style={{ backgroundColor: 'white' }}
                onClose={props.removeItem.bind(this, item.id)}
                autoClose={true}

            >
                <View style={{ flexDirection: 'row', backgroundColor: 'white', borderWidth: 1, borderRadius: 10 }}>
                    <CheckBox
                        style={styles.checkbox}
                        value={item.complete}
                        onResponderEnd={checkTaskComplete}
                        onValueChange={setSelection}
                    />
                    <Text style={styles.item}>{item.value}</Text>
                </View>
            </Swipeout>


        )
    }

    return (
        <FlatList
            onRefresh={() => setRefreshing}
            contentContainerStyle={{ flexGrow: 1, paddingBottom: 5 }}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={() => <Separator />}
            data={props.list}
            refreshing={isRefreshing}
            renderItem={renderItem}

        />


    )
}


const Separator = () => (
    <View style={styles.separator} />
);

const styles = StyleSheet.create({
    container: {
        padding: 20,
        marginTop: 30,
        flexDirection: 'row'
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'baseline'
    },
    checkboxContainer: {
        flexDirection: "row",
        marginBottom: 20,
    },
    checkbox: {
        alignSelf: 'center',
    },

    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
})

export default Todo;