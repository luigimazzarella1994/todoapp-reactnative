import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, FlatList, TouchableHighlight, ScrollView, CheckBox } from 'react-native';
import Todo from './components/Todo.js'

export default class App extends React.Component {


  state = {
    value: '',
    todoList: [],
    filteredArray: []
  }

  updateArray = (id) => {

    this.setState({ todoList: this.state.todoList.filter(todo => todo.id !== id) });
    this.setState({ filteredArray: this.state.filteredArray.filter(todo => todo.id !== id) });


  }

  onClickPressed = () => {

    if (this.state.value.trim() != '') {
      this.state.todoList.push({ value: this.state.value, complete: false, id: Math.random().toString() });
      this.setState({ filteredArray: this.state.todoList })
    }
    else {
      alert("Write something");
      return
    }
    this.setState({ value: '' });
  }

  onChangeTextHandler = (text) => {
    this.setState({ value: text });
  }

  showListFilter = (type) => {

    if (type === 'complete') {
      this.setState({ filteredArray: this.state.todoList.filter(todo => todo.complete === true) })
    }
    else if (type === 'uncomplete') {
      this.setState({ filteredArray: this.state.todoList.filter(todo => todo.complete === false) })
    } else {
      this.setState({ filteredArray: this.state.todoList })
    }
  }

  render() {

    return (
      <View>
        <StatusBar style="auto" />
        <Text style={styles.title}>TODO APP</Text>
        <View style={styles.container}>
          <TextInput
            value={this.state.value}
            onChangeText={this.onChangeTextHandler}
            style={styles.textInputContainer}
            placeholder=" Add something"
          />
          <Button
            style={styles.buttonContainer}
            title="Press Me"
            onPress={this.onClickPressed}
          />

        </View>
        <Separator />
        <View>
          <Text style={{ color: 'blue', fontSize: 18, marginLeft: 10 }}>Filter</Text>
          <View
            style={{
              flexDirection: 'row', padding: 2, margin: 10, justifyContent: 'space-between'
            }}
          >
            <Button
              style={styles.filterLeftButton}
              title="ALL"
              color='green'
              onPress={(this.showListFilter.bind(this, "All"))}
            />
            <Button
              style={styles.filterRightButton}
              title="Complete"
              onPress={this.showListFilter.bind(this, "complete")}
            />
            <Button
              style={styles.filterRightButton}
              title="Uncomplete"
              onPress={this.showListFilter.bind(this, 'uncomplete')}
            />
          </View>
        </View>
        <Separator />
        <View
          style={styles.SecondContainer}>
          <Todo list={this.state.filteredArray} removeItem={this.updateArray} />
        </View>
      </View>
    );
  }
}

const Separator = () => (
  <View style={styles.separator} />
);



const styles = StyleSheet.create({
  container: {
    padding: 20,
    marginTop: 30,
    flexDirection: 'row'
  },
  title: {
    alignContent: 'center',
    color: 'red',
    justifyContent: 'center',
    marginTop: 60,
    marginBottom: 10,
    textAlign: 'center',
    fontSize: 20

  },

  filterLeftButton: {
    backgroundColor: 'red',
    alignContent: 'flex-start',
    padding: 20,
    marginLeft: 20,
    marginRight: 20
  },


  filterRightButton: {
    alignContent: 'center',
    padding: 20,
    marginLeft: 20,
    marginRight: 20
  },

  SecondContainer: {
    margin: 10,
    padding: 10
  },
  textInputContainer: {
    padding: 10,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1, width: '70%',
    borderRadius: 10,
    marginLeft: 10,
    marginRight: 10
  },
  buttonContainer: {
    borderRadius: 10,
    borderWidth: 1,
    marginRight: 10,
  },
  separator: {
    marginVertical: 10,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
});
